/**
 * Structure de donnee
 *  [
 *      [2,"Voila les barjos"],
 *      [433,"Nul et compagnie"],
 *      [52,"Le Hobbut"],
 *      [16,"Le backwash de piscine en 52 étapes faciles"],
 *      [67,"Farenheit 867"],
 *      [1,"Le café pour les stressé"],
 *      [23,"Le pendule du barjo"],
 *      [15,"Eructer poliment"]
 *  ];
 * 
 * Algorithme
 *  Donc: 
 *      Je dois lire toutes les donnees de mon tableau.
 *      Je dois les comparer avec tout les elements de mon tableau et comparer chauqe item de mon tableau
 *      avec les autres items dans le tableau et ordonner ceux-ci selon la valeur de l'élément comparé. 
 * 
 *       Dans ma boucle, j'aurai une autre boucle imbriquée qui lira le même tableau que la boucle principale
 *       Si la valeur de mon item secondaire est plus grand que l'item de la boucle principale echanger celui-ci apres l'élément de ma boucle principale. 
 *      
 *      POUR i  TANT QUE i < LONGUEUR DU TABLEAU
 *          POUR j TANT QUE j < LONGUEUR DU TABLEAU
 *              SI j+1 existe
 *                  SI la valeur de j[0] > la valeur de j+1[0]
 *                      TEMP = tableau[j]
 *                      tableau[j] = tableau[j+1]
 *                      tableau[j+1] = TEMP
 *  
 */
let tableau = [
    [2,"Voila les barjos"],
    [433,"Nul et compagnie"],
    [52,"Le Hobbut"],
    [16,"Le backwash de piscine en 52 étapes faciles"],
    [67,"Farenheit 867"],
    [1,"Le café pour les stressé"],
    [23,"Le pendule du barjo"],
    [15,"Eructer poliment"]
  ]; 
  
  let tri_a_bulles = (tableau) => {
      let len = tableau.length;
      for (let i = 0; i<len; i++) {
          for (let j = 0; j<len; j++) {
            if(typeof tableau[j + 1] != "undefined"){
              if (tableau[j][0] > tableau[j + 1][0]) {
                  let tmp = tableau[j];
                  tableau[j] = tableau[j + 1];
                  tableau[j + 1] = tmp;
              }
            }
          }
      }
      return tableau;
  };
  
  tri_a_bulles(tableau);