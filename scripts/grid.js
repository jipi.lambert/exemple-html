let articles = $('article');
const grid = $('.grid');

$.each(articles, function (index, article) {
    const colors = ["red", "green", "blue"];
    let random_color = Math.floor(Math.random() * colors.length);
    $(article).css('background-color', colors[random_color]).attr("data-color", colors[random_color]);
    //  $(article).css('grid-column', '1/6');
});

// Vanille
// let articles = document.querySelectorAll("article");
// const colors = ["red", "green", "blue"];
// //     let random_color = Math.floor(Math.random() * colors.length); 
// articles.forEach( article => 
//     article.style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
// )

let box1 = '<article class="halves cst">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus repellat vitae minus aspernatur ex ipsam, a, delectus dicta ipsa recusandae quidem ad tempora beatae impedit, doloremque quam quia accusamus! Tenetur!</article>';
let box2 = '<article class="halves cst">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus repellat vitae minus aspernatur ex ipsam, a, delectus dicta ipsa recusandae quidem ad tempora beatae impedit, doloremque quam quia accusamus! Tenetur!</article>';
let box3 = '<article class="halves cst">Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus repellat vitae minus aspernatur ex ipsam, a, delectus dicta ipsa recusandae quidem ad tempora beatae impedit, doloremque quam quia accusamus! Tenetur!</article>';

$(box1).appendTo(grid).css('background-color', 'red').attr("data-color", "red");
$(box2).appendTo(grid).css('background-color', 'green').attr("data-color", "green");
$(box3).appendTo(grid).css('background-color', 'blue').attr("data-color", "blue");

$(".green").on('click', function (e) {
    e.preventDefault();
    $("[data-color=green]").css("background-color", "orange")
});

$(".red").on('click', function (e) {
    e.preventDefault();
    $("[data-color=red").css("background-color", "orange")
});

$(".blue").on('click', function (e) {
    e.preventDefault();
    $("[data-color=blue]").css("background-color", "orange")
});

$(".wrapper").on('click', function () {
    let grid = $(".grid");
    let max_width;

    if (grid.css('max-width') == "100%") {
        // 100% 
        max_width = "64rem";
    } else {
        max_width = "100%";
    }

    grid.animate(
    {
        "max-width": max_width
    }, 5000);


})