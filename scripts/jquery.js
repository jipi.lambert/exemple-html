$(function(){

    var autotext = $("#autotext")
    $("#clic").on("click", function(){
        $("#ici_texte").text($("#message").val());
        $("ul").append(element)
    });

    autotext.on("keyup", function(){
        $("#auto_texte").text($(this).val());
    });

    // changer la couleur de fond de ma page 
    $('body').css("background-color", "#AFEEEE");

    // cacher tout les champs input d'un formulaire
    // $('input').hide();

    // Trouver la valeur d'un tag option dans un select
    let value = $("option[value=2]").text()
    console.log(value);

    // Cocher une boite a cocher
    $("#pale").attr('checked', 'checked');

    // Trouver le second element d'une selection
    let li = $("li")[1];
    //v0
    console.log($(li).text());
    //v1
    console.log(li.innerHTML);

    // ajouter un item de liste au dbut d<un ul
    let ul_element = $("ul");
    let li_item_before = "<li>Ajout debut</li>";
    let li_item_after = "<li>Ajout fin</li>";
    ul_element.prepend(li_item_before);
    ul_element.append(li_item_after);

    // Ajout d<un div avant et apres le ul 
    ul_element.before("<div>avant UL</div>");
    ul_element.after("<div>apres UL</div>");

    // Mettre Paragraphe 1 apres le paragraphe2
    $("#p2").append($("#p1"));

    // Changer l'url d'un lien
    $("#lien").attr("href","#lien");
    $("#lien").text("Ceci est un lien");

    // Ajouter une classe a un element 
    let change = $("#change");
    change.addClass('uneclass');

    //Trouver la hauteur et la largeur de mon element
    console.log(change.height());
    console.log(change.width());
    //trouver la position de mon element
    console.log(change.position());
    console.log(change.position().top);
    console.log(change.position().left);

    //Trouver le decalage d'un element
    let offset = $(".offset");
    console.log(offset.offset())
    console.log(offset.offset().top)
    console.log(offset.offset().left)
    
    offset.animate({
        marginLeft:"+=100"
    }, 5000);

    $("#top").on("click", function(){
        $(window).scrollTop(0); 
    });
    //Utiliser Jquery et le css avance
    $('li:odd').addClass("dark");
    $('li:even').addClass("light");
});